(function($){
	// Settings
	var repeat = localStorage.repeat || 0,
		shuffle = localStorage.shuffle || 'false',
		continous = true,
		autoplay = true,
		playlist = [
		        {
				title: '光辉岁月',
				artist: 'beyond',
				album: '命运派对',
				cover: 'img/4.jpg',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7344962158851148563.mp3',
				ogg: ''
			},
			{
				title: '海阔天空',
				artist: 'beyond',
				album: '乐与怒',
				cover:'img/10.jpg',
				mp3: 'http://music.163.com/song/media/outer/url?id=346089.mp3',
				ogg: ''
			},
			{
				title: '真的爱你',
				artist: 'beyond',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000000eOgmK1fN8Cs_2.jpg?max_age=2592000',
				mp3: 'http://music.163.com/song/media/outer/url?id=469391955.mp3',
				ogg: ''
			},
			{
				title: '灰色轨迹',
				artist: 'beyond',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000004EOVA73heGrc_1.jpg?max_age=2592000',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7330546058054961970.mp3',
				ogg: ''
			},
			{
				title: '情人',
				artist: 'beyond',
				album: '',
				cover:'img/10.jpg',
				mp3: 'http://music.163.com/song/media/outer/url?id=469391953.mp3',
				ogg: ''
			},
			{
				title: '无尽空虚',
				artist: 'beyond',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000000fRELw0ShGt1_1.jpg?max_age=2592000',
				mp3: 'http://music.163.com/song/media/outer/url?id=346591.mp3',
				ogg: ''
			},
			{
				title: '不再犹豫',
				artist: 'beyond',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000001Gikfw1MiLRm_2.jpg?max_age=2592000',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7238560853552057146.mp3?testst=1738778762337',
				ogg: ''
			},
			{
				title: '喜欢你',
				artist: 'beyond',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000001oHxZZ1pAQn4_2.jpg?max_age=2592000',
				mp3: 'http://music.163.com/song/media/outer/url?id=346163.mp3',
				ogg: ''
			},
			{
				title: '冷雨夜',
				artist: 'beyond',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000002MGTsZ3Zl6JH_2.jpg?max_age=2592000',
				mp3: 'http://music.163.com/song/media/outer/url?id=32701154.mp3',
				ogg: ''
			},
		        {
				title: '斑马随弹',
				artist: 'saillin',
				album: '',
				cover:'https://saillin.gitlab.io/camera/images/thumbs/34.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/斑马随弹.mp3',
				ogg: ''
			},
			{
				title: '不是因为寂寞才想你',
				artist: 'saillin',
				album: '',
				cover:'https://saillin.gitlab.io/camera/images/thumbs/15.jpg',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7279695286786673464.mp3',
				ogg: ''
			},
			{
				title: '成都练习一点点',
				artist: 'saillin',
				album: '',
				cover:'https://saillin.gitlab.io/camera/images/thumbs/35.jpg',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7278957559386082108.mp3',
				ogg: ''
			},
			{
				title: '挪威的森林',
				artist: '伍佰',
				album: '滚石香港黄金十年 伍佰精选',
				cover: 'img/2.jpg',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7151693872249965342.mp3',
				ogg: ''
			},
			{
				title: '突然的自我 ',
				artist: '伍佰',
				album: '',
				cover:'img/2.jpg',
				mp3: 'https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/7083060662196374285.mp3',
				ogg: ''
			},
			{
				title: 'chenparty dj',
				artist: '德国童声',
				album: 'chenparty 超好听的德国童声 dj',
				cover: 'img/3.jpg',
				mp3: 'http://rm.sina.com.cn/wm/VZ2010050511043310440VK/music/MUSIC1005051622027270.mp3',
				ogg: ''
			},
                        {
				title: '不浪漫罪名',
				artist: '王杰',
				album: '万岁2001',
				cover:'img/5.jpg',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/93e7886c04cc40dbace3280e8cd01f6b',
				ogg: ''
			},
			{
				title: '谁明浪子心',
				artist: '王杰',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000003mwGsZ4OHBk4_3.jpg?max_age=2592000',
				mp3: 'http://music.163.com/song/media/outer/url?id=1315217468.mp3',
				ogg: ''
			},
			{
				title: '忘了你忘了我',
				artist: '王杰',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000004TW0qt4eafRy_2.jpg?max_age=2592000',
				mp3: 'https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/7073317031361170183.mp3',
				ogg: ''
			},
			{
				title: '我是真的爱上你',
				artist: '王杰',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M0000038ogD80A3PUH_2.jpg?max_age=2592000',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/我是真的爱上你.mp3',
				ogg: ''
			},
			{
				title: '一场游戏一场梦',
				artist: '王杰',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M0000026DG8k3Fw488_3.jpg?max_age=2592000',
				mp3: 'http://music.163.com/song/media/outer/url?id=402073160.mp3',
				ogg: ''
			},
                        {
				title: '安和桥',
				artist: '宋冬野',
				album: '安和桥北',
				cover:'img/6.jpg',
				mp3: 'http://music.163.com/song/media/outer/url?id=27646205.mp3',
				ogg: ''
			},
			{
				title: '斑马斑马',
				artist: '宋冬野',
				album: '安和桥北',
				cover:'img/6.jpg',
				mp3: 'http://music.163.com/song/media/outer/url?id=27646199.mp3',
				ogg: ''
			},
			{
				title: '明年今日',
				artist: '陈奕迅',
				album: '',
				cover:'http://p2.music.126.net/ZtQenXNwYCEMjh7hJEL47g==/109951163381546710.jpg?param=130y130',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/6b52f23c1b2846489cb0bc39c560276b',
				ogg: ''
			},
			{
				title: '浮夸',
				artist: '陈奕迅',
				album: '',
				cover:'http://p2.music.126.net/xuFy0k8O_xKuAqbbjC24Ig==/109951166497586944.jpg?param=130y130',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7336328499264867091.mp3',
				ogg: ''
			},
			{
				title: '富士山下',
				artist: '陈奕迅',
				album: '',
				cover:'http://p1.music.126.net/jzNxBp5DCER2_aKGsXeRww==/109951167435823724.jpg?param=130y130',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7335842304550292262.mp3',
				ogg: ''
			},
			{
				title: '认真的雪',
				artist: '薛之谦',
				album: '',
				cover:'http://p2.music.126.net/iSfHsgwCPXyvIC-9b3Oytw==/109951165625500222.jpg?param=130y130',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7274920205686410044.mp3',
				ogg: ''
			},
			{
				title: '绅士',
				artist: '薛之谦',
				album: '',
				cover:'http://p2.music.126.net/vu7SHbVlMuszmSuKR2SKAQ==/109951168707343730.jpg?param=130y130',
				mp3: 'https://sf27-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/8f6b18d1904e4f5dbfbd9cad4357829a',
				ogg: ''
			},
			{
				title: '演员',
				artist: '薛之谦',
				album: '',
				cover:'http://p2.music.126.net/vu7SHbVlMuszmSuKR2SKAQ==/109951168707343730.jpg?param=130y130',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/55aaf4686e22490098cab1ced466d402',
				ogg: ''
			},
			{
				title: '夜曲',
				artist: '周杰伦',
				album: '',
				cover:'https://p1.music.126.net/yEGx5yAd9zaa3l03d30KNw==/18511377767183740.jpg?param=200y200',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7087197126248008456.mp3',
				ogg: ''
			},
			{
				title: '晴天',
				artist: '周杰伦',
				album: '',
				cover:'http://p2.music.126.net/DpAuXTXlirLKXawYxJo70g==/109951164579389026.jpg?param=130y130',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7229517242101271356.mp3',
				ogg: ''
			},
			{
				title: '蒲公英的约定',
				artist: '周杰伦',
				album: '',
				cover:'http://imge.kugou.com/stdmusic/20220705/20220705143006989099.jpg',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7084160583003573006.mp3',
				ogg: ''
			},
			{
				title: '明明就',
				artist: '周杰伦',
				album: '',
				cover:'http://imge.kugou.com/stdmusic/20231008/20231008095605522305.jpg',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7071529902398753550.mp3',
				ogg: ''
			},
			{
				title: '枫',
				artist: '周杰伦',
				album: '',
				cover:'http://imge.kugou.com/stdmusic/20150720/20150720160716937643.jpg',
				mp3: 'https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/7136805046540700429.mp3',
				ogg: ''
			},
                        {
				title: '不找了',
				artist: '郭旭',
				album: '',
				cover:'img/7.jpg',
				mp3: 'http://music.163.com/song/media/outer/url?id=29850531.mp3',
				ogg: ''
			},
                        {
				title: '钟无颜',
				artist: '谢安琪',
				album: '',
				cover:'img/8.jpg',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7386678571303209782.mp3?testst=1738777387789',
				ogg: ''
			},
                        {
				title: 'Beautiful Times',
				artist: 'Owl City',
				album: 'Beautiful Times',
				cover:'img/9.jpg',
				mp3: 'http://music.163.com/song/media/outer/url?id=29543482.mp3',
				ogg: ''
			},
                        {
				title: '你不是真正的快乐',
				artist: '五月天',
				album: '',
				cover:'img/11.jpg',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7058200245368343303.mp3',
				ogg: ''
			},
			{
				title: '突然好想你',
				artist: '五月天',
				album: '',
				cover:'img/11.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/突然好想你.mp3',
				ogg: ''
			},
			{
				title: '知足',
				artist: '五月天',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000003PIMo40rxcAn_1.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/知足.mp3',
				ogg: ''
			},
			{
				title: '我不愿让你一个人',
				artist: '五月天',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000001fbipy4azgKM_2.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/我不愿让你一个人.mp3',
				ogg: ''
			},
			{
				title: '步步',
				artist: '五月天',
				album: '',
				cover:'http://p2.music.126.net/7g0QYe0twjfOeCCWy9qxMw==/109951168144493672.jpg?param=130y130',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7078685717034994439.mp3',
				ogg: ''
			},
			{
				title: '温柔',
				artist: '五月天',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M0000042miPf28nzum_1.jpg',
				mp3: 'http://music.163.com/song/media/outer/url?id=5237943.mp3',
				ogg: ''
			},
                        {
				title: '断点',
				artist: '张敬轩',
				album: '',
				cover:'img/12.jpg',
				mp3: 'https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/7147971359849728805.mp3',
				ogg: ''
			},
                        {
				title: '爱你',
				artist: '陈芳语',
				album: '',
				cover:'img/13.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/陈芳语-爱你.mp3',
				ogg: ''
                        },
                        {
				title: '去年夏天',
				artist: '王大毛',
				album: '',
				cover:'img/14.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/去年夏天.mp3',
				ogg: ''
			},
                        {
				title: '去年夏天',
				artist: '家家',
				album: '',
				cover:'img/15.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/去年夏天1.mp3',
				ogg: ''
			},
                        {
				title: '有点甜',
				artist: '泥鳅Niko,小泥丸',
				album: '',
				cover:'img/16.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/有点甜.mp3',
				ogg: ''
			},
                        {
				title: '喜欢你',
				artist: 'G.E.M.邓紫棋',
				album: '',
				cover:'img/18.jpg',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7109834303311973150.mp3?testst=1738777460165',
				ogg: ''
			},
                        {
				title: 'My Love',
				artist: 'Westlife',
				album: '',
				cover:'img/19.jpg',
				mp3: 'http://music.163.com/song/media/outer/url?id=2080749.mp3',
				ogg: ''
			},
                        {
				title: '穿越时空的思念',
				artist: 'DiESi',
				album: '',
				cover:'img/20.jpg',
				mp3: 'http://music.163.com/song/media/outer/url?id=1345395655.mp3',
				ogg: ''
			},
                        {
				title: '天空之城（经典钢琴版）',
				artist: '（翻自 久石譲） - dylanf',
				album: '',
				cover:'img/21.jpg',
				mp3: 'http://music.163.com/song/media/outer/url?id=864711417.mp3',
				ogg: ''
			},
                        {
				title: '模特',
				artist: '李荣浩',
				album: '',
				cover:'img/22.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/模特.mp3',
				ogg: ''
			},
			{
				title: '不将就',
				artist: '李荣浩',
				album: '',
				cover:'img/40.jpg',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7420794509468306213.mp3',
				ogg: ''
			},
                        {
				title: '你的酒馆对我打了烊',
				artist: '陈雪凝',
				album: '',
				cover:'img/23.jpg',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7320244562566433575.mp3',
				ogg: ''
			},
                        {
				title: '绿色',
				artist: '陈雪凝',
				album: '',
				cover:'img/24.jpg',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7359015109789846309.mp3',
				ogg: ''
			},
                        {
				title: '灰姑娘',
				artist: '陈雪凝',
				album: '',
				cover:'img/25.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/灰姑娘.mp3',
				ogg: ''
			},
                        {
				title: '成都',
				artist: '赵雷',
				album: '',
				cover:'img/26.jpg',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7416333634888436491.mp3?testst=1738777620908',
				ogg: ''
			},
                        {
				title: '理想',
				artist: '赵雷',
				album: '',
				cover:'img/27.jpg',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7157680125597338399.mp3',
				ogg: ''
			},
                        {
				title: '小幸运',
				artist: '田馥甄',
				album: '',
				cover:'img/28.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/小幸运.mp3',
				ogg: ''
			},
                        {
				title: '寂寞寂寞就好',
				artist: '田馥甄',
				album: '',
				cover:'img/29.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/寂寞寂寞就好.mp3',
				ogg: ''
			},
                        {
				title: '你就不要想起我',
				artist: '田馥甄',
				album: '',
				cover:'img/30.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/你就不要想起我.mp3',
				ogg: ''
			},
			{
				title: '爱你',
				artist: '王心凌',
				album: '',
				cover:'img/31.jpg',
				mp3: 'http://music.163.com/song/media/outer/url?id=2067672521.mp3',
				ogg: ''
			},
			{
				title: '红色高跟鞋',
				artist: '蔡健雅',
				album: '',
				cover:'img/32.jpg',
				mp3: 'https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/7036693201088219941.mp3',
				ogg: ''
			},
			{
				title: '曾经的你',
				artist: '许巍',
				album: '',
				cover:'img/33.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/许巍-曾经的你.mp3',
				ogg: ''
			},
			{
				title: '蓝莲花',
				artist: '许巍',
				album: '',
				cover:'img/34.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/许巍-蓝莲花.mp3',
				ogg: ''
			},
			{
				title: '时光',
				artist: '许巍',
				album: '',
				cover:'img/34.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/许巍-时光.mp3',
				ogg: ''
			},
			{
				title: '没有你陪伴真的好孤单',
				artist: '梦然',
				album: '',
				cover:'img/35.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/没有你陪伴真的好孤单.mp3',
				ogg: ''
			},
			{
				title: '是你',
				artist: '梦然',
				album: '',
				cover:'img/39.jpg',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7150314616214522655.mp3',
				ogg: ''
			},
			{
				title: '少年',
				artist: '梦然',
				album: '',
				cover:'img/35.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/少年.mp3',
				ogg: ''
			},
			{
				title: 'Let Her Go',
				artist: 'Passenger',
				album: '',
				cover:'img/36.jpg',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/6884141511932791566.mp3?testst=1738777765017',
				ogg: ''
			},
			{
				title: '哪里都是你',
				artist: '队长',
				album: '',
				cover:'img/37.jpg',
				mp3: 'http://music.163.com/song/media/outer/url?id=488249475.mp3',
				ogg: ''
			},
			{
				title: '你？好不好',
				artist: '周兴哲',
				album: '',
				cover:'img/38.jpg',
				mp3: 'https://gitlab.com/saillin/saillin.gitlab.io/-/raw/master/content/music/gedan/你？好不好.mp3',
				ogg: ''
			},
			{
				title: '他不懂',
				artist: '张杰',
				album: '',
				cover:'http://p1.music.126.net/mW53BkMgGy37I7yVrUg-aQ==/109951163117902077.jpg?param=130y130',
				mp3: 'http://music.163.com/song/media/outer/url?id=28059417.mp3',
				ogg: ''
			},
			{
				title: '后来',
				artist: '刘若英',
				album: '',
				cover:'http://p2.music.126.net/eBF7bHnJYBUfOFrJ_7SUfw==/109951163351825356.jpg?param=177y177',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7127146840323377950.mp3',
				ogg: ''
			},
			{
				title: '我好喜欢你',
				artist: '六哲',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000000PZ0XZ15awZN_1.jpg?max_age=2592000',
				mp3: 'https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/7225275201477462844.mp3',
				ogg: ''
			},
			{
				title: '我好喜欢你',
				artist: '半吨兄弟',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000001z5nyk0UBUC1_1.jpg?max_age=2592000',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7177261320843316028.mp3',
				ogg: ''
			},
			{
				title: '错位时空',
				artist: '艾辰',
				album: '',
				cover:'http://p1.music.126.net/8C0lwLE88j9ZwLyPQ9a4FA==/109951165595770076.jpg?param=130y130',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7082288812583291679.mp3',
				ogg: ''
			},
			{
				title: '当我取过她',
				artist: '莫叫姐姐',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000002Qk9s90OojOj_2.jpg?max_age=2592000',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7097003369042070308.mp3',
				ogg: ''
			},
			{
				title: '白月光与朱砂痣',
				artist: '胖虎',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000003Yl2rh0jhPoa_1.jpg?max_age=2592000',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7235153710404438845.mp3',
				ogg: ''
			},
			{
				title: '银河与星斗',
				artist: 'Emily酱',
				album: '',
				cover:'http://p1.music.126.net/22HX7teUQcIWCgx4KWk0rw==/109951165246588434.jpg?param=640y300',
				mp3: 'http://music.163.com/song/media/outer/url?id=1853878942.mp3',
				ogg: ''
			},
			{
				title: '后来的你在哪',
				artist: '树泽',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000000fAlQD2boWLu_5.jpg?max_age=2592000',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7115805885093694215.mp3',
				ogg: ''
			},
			{
				title: 'RingRingRing',
				artist: '璃露',
				album: '',
				cover:'http://p1.music.126.net/da8ZoTzKR9-akPRWFPKllA==/109951164371835276.jpg?param=130y130',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/6979647239660505887.mp3',
				ogg: ''
			},
			{
				title: '清空',
				artist: '麦小兜',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M0000015q2sz20N5Pm_2.jpg?max_age=2592000',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7031728698000870174.mp3',
				ogg: ''
			},
			{
				title: '目及皆是你',
				artist: '小蓝背心',
				album: '',
				cover:'http://p2.music.126.net/jDTEtAnZiDvfq5iBpLQBRA==/109951166171265193.jpg?param=177y177',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/6996787049030257439.mp3',
				ogg: ''
			},
			{
				title: '江南',
				artist: '林俊杰',
				album: '',
				cover:'http://p1.music.126.net/_0OAhWhIbg-nOP-6e4o-SA==/109951168111265583.jpg?param=130y130',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7139474312634518308.mp3',
				ogg: ''
			},
			{
				title: '孤城',
				artist: '洛洛',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000004A9M1H0sQZ6d_2.jpg?max_age=2592000',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7237586084639689532.mp3',
				ogg: ''
			},
			{
				title: 'Pastlives',
				artist: 'Sapiedream',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T002R300x300M000000casLm2tGU9U_1.jpg?max_age=2592000',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7321004458195127091.mp3',
				ogg: ''
			},
			{
				title: '有何不可',
				artist: '许嵩',
				album: '',
				cover:'http://p2.music.126.net/eZDYCtPQ3dyO2yt222x-4A==/109951168634474582.jpg?param=130y130',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/8b03653561014353bee6bea90942075c',
				ogg: ''
			},
			{
				title: '素颜',
				artist: '许嵩',
				album: '',
				cover:'http://p2.music.126.net/eZDYCtPQ3dyO2yt222x-4A==/109951168634474582.jpg?param=130y130',
				mp3: 'https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/7005366362612828941.mp3',
				ogg: ''
			},
			{
				title: '千百度',
				artist: '许嵩',
				album: '',
				cover:'http://p2.music.126.net/fUEbmHuK22dQkUcIWiA0JA==/3352410953143856.jpg?param=130y130',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/1654480857969700.mp3',
				ogg: ''
			},
			{
				title: '梦的翅膀受了伤dj',
				artist: '精彩轩迪',
				album: '',
				cover:'http://p1.music.126.net/xg5TPuk_DoRGL6eXQ_Lc7g==/109951168745094057.jpg?param=130y130',
				mp3: 'https://sf6-cdn-tos.douyinstatic.com/obj/ies-music/6993712525435046692.mp3',
				ogg: ''
			},
			{
				title: '夏天的风',
				artist: '温岚',
				album: '',
				cover:'http://p1.music.126.net/ZcQbKfT18h2Twyu5QwJInA==/109951167598330468.jpg?param=130y130',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/27a60b6d709a40e2bd358a80cc759ac1',
				ogg: ''
			},
			{
				title: 'bingbian',
				artist: '鞠文娴',
				album: '',
				cover:'http://p2.music.126.net/eZDYCtPQ3dyO2yt222x-4A==/109951168634474582.jpg?param=130y130',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7285635595899341625.mp3',
				ogg: ''
			},
			{
				title: '余香',
				artist: 'null',
				album: '',
				cover:'http://p2.music.126.net/eZDYCtPQ3dyO2yt222x-4A==/109951168634474582.jpg?param=130y130',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7027018065044884231.mp3',
				ogg: ''
			},
			{
				title: '偏爱',
				artist: 'dj小姐姐',
				album: '',
				cover:'http://p2.music.126.net/eZDYCtPQ3dyO2yt222x-4A==/109951168634474582.jpg?param=130y130',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/dab7de981c864d3a9ffb6398b5757d1a',
				ogg: ''
			},
			{
				title: '在你的身边',
				artist: '慢热气球',
				album: '',
				cover:'http://p1.music.126.net/_iGPqQDnM_3Yz8hVAPtcTw==/109951167675894788.jpg?param=130y130',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7154669760221924132.mp3',
				ogg: ''
			},
			{
				title: '最后一页',
				artist: '洛尘鞅',
				album: '',
				cover:'http://p2.music.126.net/5ea2ojVfB0kW4dyi-edpXw==/109951167184544287.jpg?param=130y130',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7273460765976333117.mp3',
				ogg: ''
			},
			{
				title: 'Shots',
				artist: 'Broiler',
				album: '',
				cover:'http://p1.music.126.net/pCggmlokub94Lxop1a3KOQ==/2942293116060555.jpg?param=130y130',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7159186413949373221.mp3',
				ogg: ''
			},
			{
				title: '可不可以',
				artist: '张紫豪',
				album: '',
				cover:'http://p2.music.126.net/GRvXpp7i0TbbsbJncl4A7g==/109951163594165998.jpg?param=130y130',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7032610316751997726.mp3',
				ogg: ''
			},
			{
				title: '删了吧',
				artist: '烟(许佳豪)',
				album: '',
				cover:'http://p1.music.126.net/nNg4YjkcK1AwCX1FrN8VOQ==/109951166578333625.jpg?param=130y130',
				mp3: 'http://music.163.com/song/media/outer/url?id=1891469546.mp3',
				ogg: ''
			},
			{
				title: '暖一杯茶',
				artist: '邵帅',
				album: '',
				cover:'http://p2.music.126.net/dAP3RXAs9dA73zNYz_3XSg==/109951164151547523.jpg?param=130y130',
				mp3: 'http://music.163.com/song/media/outer/url?id=1371780785.mp3',
				ogg: ''
			},
			{
				title: '专属情歌',
				artist: '刘辰希',
				album: '',
				cover:'http://p2.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg?param=640y300',
				mp3: 'https://sf3-cdn-tos.douyinstatic.com/obj/ies-music/7357401067723705125.mp3',
				ogg: ''
			},
			{
				title: '追光者',
				artist: '岑宁儿',
				album: '',
				cover:'http://p2.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg?param=640y300',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7056683639387573004.mp3',
				ogg: ''
			},
			{
				title: '未必',
				artist: '言瑾羽',
				album: '',
				cover:'http://p2.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg?param=640y300',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7342096806743968553.mp3',
				ogg: ''
			},
			{
				title: '冬眠',
				artist: '司南',
				album: '',
				cover:'http://p2.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg?param=640y300',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/6909006653623667464.mp3',
				ogg: ''
			},
			{
				title: '往事清零',
				artist: '',
				album: '',
				cover:'http://p2.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg?param=640y300',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7100988415927126815.mp3',
				ogg: ''
			},
			{
				title: '有形的翅膀',
				artist: '',
				album: '',
				cover:'http://p1.music.126.net/pcic3NwD7xdvHLvuT5jWWQ==/109951168308921434.jpg?param=130y130',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7360512339172576027.mp3',
				ogg: ''
			},
			{
				title: '姑娘别哭泣',
				artist: '',
				album: '',
				cover:'https://p3.douyinpic.com/aweme/200x200/tos-cn-v-2774c002/oIutnDfeVEzcwJAC6BAgNshCk0EBZHLBQAWgHB.jpeg',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/tos-cn-ve-2774/okfQadPMDeFXK6gAYzFngeKcD2HFqPJ0sDDbCW',
				ogg: ''
			},
			{
				title: 'free loop',
				artist: ' Daniel Powter',
				album: '',
				cover:'https://p11.douyinpic.com/aweme/200x200/tos-cn-v-2774c002/a7f2ffe87b2647b68290b80f0e0a7025.jpeg',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7178754183816907577.mp3',
				ogg: ''
			},
			{
				title: '爱，很简单',
				artist: '陶喆',
				album: '',
				cover:'https://p11.douyinpic.com/aweme/200x200/tos-cn-v-2774c002/508f1f2a0c6b498c9426fcf1a9a47ff8.jpeg',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7206831189296974653.mp3',
				ogg: ''
			},
			{
				title: '与我无关',
				artist: '阿冗',
				album: '',
				cover:'https://p26.douyinpic.com/aweme/200x200/tos-cn-v-2774c002/683cf473712d45c793237e9359158cb2.jpeg',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7402263041338772250.mp3',
				ogg: ''
			},
			{
				title: '孤单心事',
				artist: '蓝又时',
				album: '',
				cover:'https://y.qq.com/music/photo_new/T001R150x150M000003TCaV12AznGx.jpg?max_age=2592000',
				mp3: 'https://sf5-hl-cdn-tos.douyinstatic.com/obj/ies-music/7405774401477643035.mp3?testst=1738776861562',
				ogg: ''
			},
			{
				title: '最后一页抖音版',
				artist: '洛尘鞅',
				album: '',
				cover:'http://p2.music.126.net/5ea2ojVfB0kW4dyi-edpXw==/109951167184544287.jpg?param=130y130',
				mp3: 'http://music.163.com/song/media/outer/url?id=445154140.mp3',
				ogg: ''
			},
			
			];
	// Load playlist
	for (var i=0; i<playlist.length; i++){
		var item = playlist[i];
		$('#playlist').append('<li>'+item.artist+' - '+item.title+'</li>');
	}

	var time = new Date(),
		currentTrack = shuffle === 'true' ? time.getTime() % playlist.length : 0,
		trigger = false,
		audio, timeout, isPlaying, playCounts;

	var play = function(){
		audio.play();
		$('.playback').addClass('playing');
		timeout = setInterval(updateProgress, 500);
		isPlaying = true;
	}

	var pause = function(){
		audio.pause();
		$('.playback').removeClass('playing');
		clearInterval(updateProgress);
		isPlaying = false;
	}

	// Update progress
	var setProgress = function(value){
		var currentSec = parseInt(value%60) < 10 ? '0' + parseInt(value%60) : parseInt(value%60),
			ratio = value / audio.duration * 100;

		$('.timer').html(parseInt(value/60)+':'+currentSec);
		$('.progress .pace').css('width', ratio + '%');
		$('.progress .slider a').css('left', ratio + '%');
	}

	var updateProgress = function(){
		setProgress(audio.currentTime);
	}

	// Progress slider
	$('.progress .slider').slider({step: 0.1, slide: function(event, ui){
		$(this).addClass('enable');
		setProgress(audio.duration * ui.value / 100);
		clearInterval(timeout);
	}, stop: function(event, ui){
		audio.currentTime = audio.duration * ui.value / 100;
		$(this).removeClass('enable');
		timeout = setInterval(updateProgress, 500);
	}});

	// Volume slider
	var setVolume = function(value){
		audio.volume = localStorage.volume = value;
		$('.volume .pace').css('width', value * 100 + '%');
		$('.volume .slider a').css('left', value * 100 + '%');
	}

	var volume = localStorage.volume || 0.5;
	$('.volume .slider').slider({max: 1, min: 0, step: 0.01, value: volume, slide: function(event, ui){
		setVolume(ui.value);
		$(this).addClass('enable');
		$('.mute').removeClass('enable');
	}, stop: function(){
		$(this).removeClass('enable');
	}}).children('.pace').css('width', volume * 100 + '%');

	$('.mute').click(function(){
		if ($(this).hasClass('enable')){
			setVolume($(this).data('volume'));
			$(this).removeClass('enable');
		} else {
			$(this).data('volume', audio.volume).addClass('enable');
			setVolume(0);
		}
	});

	// Switch track
	var switchTrack = function(i){
		if (i < 0){
			track = currentTrack = playlist.length - 1;
		} else if (i >= playlist.length){
			track = currentTrack = 0;
		} else {
			track = i;
		}

		$('audio').remove();
		loadMusic(track);
		if (isPlaying == true) play();
	}

	// Shuffle
	var shufflePlay = function(){
		var time = new Date(),
			lastTrack = currentTrack;
		currentTrack = time.getTime() % playlist.length;
		if (lastTrack == currentTrack) ++currentTrack;
		switchTrack(currentTrack);
	}

	// Fire when track ended
	var ended = function(){
		pause();
		audio.currentTime = 0;
		playCounts++;
		if (continous == true) isPlaying = true;
		if (repeat == 1){
			play();
		} else {
			if (shuffle === 'true'){
				shufflePlay();
			} else {
				if (repeat == 2){
					switchTrack(++currentTrack);
				} else {
					if (currentTrack < playlist.length) switchTrack(++currentTrack);
				}
			}
		}
	}

	var beforeLoad = function(){
		var endVal = this.seekable && this.seekable.length ? this.seekable.end(0) : 0;
		$('.progress .loaded').css('width', (100 / (this.duration || 1) * endVal) +'%');
	}

	// Fire when track loaded completely
	var afterLoad = function(){
		if (autoplay == true) play();
	}

	// Load track
	var loadMusic = function(i){
		var item = playlist[i],
			newaudio = $('<audio>').html('<source src="'+item.mp3+'"><source src="'+item.ogg+'">').appendTo('#player');
		
		$('.cover').html('<img src="'+item.cover+'" alt="'+item.album+'">');
		$('.tag').html('<strong>'+item.title+'</strong><span class="artist">'+item.artist+'</span><span class="album">'+item.album+'</span>');
		$('#playlist li').removeClass('playing').eq(i).addClass('playing');
		audio = newaudio[0];
		audio.volume = $('.mute').hasClass('enable') ? 0 : volume;
		audio.addEventListener('progress', beforeLoad, false);
		audio.addEventListener('durationchange', beforeLoad, false);
		audio.addEventListener('canplay', afterLoad, false);
		audio.addEventListener('ended', ended, false);
	}

	loadMusic(currentTrack);
	$('.playback').on('click', function(){
		if ($(this).hasClass('playing')){
			pause();
		} else {
			play();
		}
	});
	$('.rewind').on('click', function(){
		if (shuffle === 'true'){
			shufflePlay();
		} else {
			switchTrack(--currentTrack);
		}
	});
	$('.fastforward').on('click', function(){
		if (shuffle === 'true'){
			shufflePlay();
		} else {
			switchTrack(++currentTrack);
		}
	});
	$('#playlist li').each(function(i){
		var _i = i;
		$(this).on('click', function(){
			switchTrack(_i);
		});
	});

	if (shuffle === 'true') $('.shuffle').addClass('enable');
	if (repeat == 1){
		$('.repeat').addClass('once');
	} else if (repeat == 2){
		$('.repeat').addClass('all');
	}

	$('.repeat').on('click', function(){
		if ($(this).hasClass('once')){
			repeat = localStorage.repeat = 2;
			$(this).removeClass('once').addClass('all');
		} else if ($(this).hasClass('all')){
			repeat = localStorage.repeat = 0;
			$(this).removeClass('all');
		} else {
			repeat = localStorage.repeat = 1;
			$(this).addClass('once');
		}
	});

	$('.shuffle').on('click', function(){
		if ($(this).hasClass('enable')){
			shuffle = localStorage.shuffle = 'false';
			$(this).removeClass('enable');
		} else {
			shuffle = localStorage.shuffle = 'true';
			$(this).addClass('enable');
		}
	});
})(jQuery);

var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?9d2f00b533f9cca146f784443e5bfc96";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
